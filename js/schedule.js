/* Спрятать, если не включен JS */
document.getElementById("demo").style.display = "block";
document.getElementById("date").style.display = "block";
document.getElementById("month").style.display = "block";
document.getElementById("text").style.display = "block";
document.getElementById("details").style.display = "block";
/* Переменные */
const d = new Date();
const day = d.getDay();
const date = d.getUTCDate();
let month = d.getMonth();
/* Основная часть */
document.getElementById("date").innerHTML = date;
const days = [
  "Воскресенье,",
  "Понедельник,",
  "Вторник,",
  "Среда,",
  "Четверг,",
  "Пятница,",
  "Суббота,"
];
document.getElementById("demo").innerHTML = days[day];
/* Надо переделать, чтобы информация бралась из json */
const schedule = [
  'Выходной <img src="/images/beach.svg" alt="beach" class="emoji" style="bottom: 4px; position: relative">',
  '<b>09:00-10:30</b> Оркестровый класс <img class="emoji" src="/images/accordion.svg" alt="accordion"><img class="emoji" src="/images/doll.svg" alt="doll"><br><b>09:00-11:20</b> Хоровой класс <img class="emoji" src="/images/wand.svg" alt="wand"> (408)<br><b>10:45-12:15</b> Физическая культура (Спортивный зал) <img class="emoji" src="/images/violin.svg" alt="violin"><br><b>16:45-18:15</b> Литература (208) <img class="emoji" src="/images/together.svg" alt="together">',
  '<b>09:00-10:30</b> Сольфеджио (315) <img class="emoji" src="/images/trumpet.svg" alt="trumpet"><img class="emoji" src="/images/drum.svg" alt="drum"><img class="emoji" src="/images/accordion.svg" alt="accordion"><img class="emoji" src="/images/doll.svg" alt="doll"><br><b>09:00-11:20</b> Хоровой класс <img class="emoji" src="/images/wand.svg" alt="micro"> (БЗ)<br><b>13:00-14:30</b> История мировой культуры (7) <img class="emoji" src="/images/together.svg" alt="together"><br><b>14:45-16:15</b> ОБЖ (309) <img class="emoji" src="/images/together.svg" alt="together"><br><b>14:30-17:45</b> Оркестровый класс <img class="emoji" src="/images/violin.svg" alt="violin">',
  '<b>09:00-10:30</b> Элементарная теория музыки (309) <img class="emoji" src="/images/together.svg" alt="together"><br><b>10:45-12:15</b> История (511) <img class="emoji" src="/images/together.svg" alt="together"><br><b>13:00-14:30</b> Сольфеджио (103а) <img class="emoji" src="/images/piano.svg" alt="piano"><img class="emoji" src="/images/violin.svg" alt="violin"><img class="emoji" src="/images/wand.svg" alt="micro"><br><b>13:00-16:30</b> Оркестровый класс<img class="emoji" src="/images/trumpet.svg" alt="trumpet"><img class="emoji" src="/images/drum.svg" alt="drum"><br><b>16:45-18:15</b> Русский язык (309) <img class="emoji" src="/images/together.svg" alt="together">',
  '<b>09:00-11:20</b> Оркестровый класс <img class="emoji" src="/images/accordion.svg" alt="accordion"><img class="emoji" src="/images/doll.svg" alt="doll"><br><b>09:00-11:20</b> Хоровой класс (408) <img class="emoji" src="/images/wand.svg" alt="micro"><br><b>13:00-14:30</b> Музыкальная литература (511) <img class="emoji" src="/images/together.svg" alt="together"><br><b>14:30-17:45</b> Оркестровый класс <img class="emoji" src="/images/violin.svg" alt="violin"><br><b>14:45-16:30</b> Физическая культура <img class="emoji" src="/images/piano.svg" alt="piano"><img class="emoji" src="/images/trumpet.svg" alt="trumpet"><img class="emoji" src="/images/drum.svg" alt="drum"><img class="emoji" src="/images/accordion.svg" alt="accordion"><img class="emoji" src="/images/doll.svg" alt="doll"><img class="emoji" src="/images/wand.svg" alt="micro">',
  '<b>13:00-13:45</b> Музыкальная литература <img class="emoji" src="/images/together.svg" alt="together"><br><b>13:50-14:35</b> Народная музыкальная культура <img class="emoji" src="/images/together.svg" alt="together">',
  '<b>9:00-11:20</b> Оркестровый класс <img class="emoji" src="/images/accordion.svg" alt="accordion"><img class="emoji" src="/images/doll.svg" alt="doll"><br><b>11:00-14:45</b> Оркестровый класс <img class="emoji" src="/images/trumpet.svg" alt="trumpet"><img class="emoji" src="/images/drum.svg" alt="drum"><br><b>13:00-14:30</b> Иностранный язык (511)<img class="emoji" src="/images/together.svg" alt="together"><br><b>14:45-15:30</b> Математика (309) <img class="emoji" src="/images/together.svg" alt="together"><br><b>15:35-17:10</b> Естествознание (309) <img class="emoji" src="/images/together.svg" alt="together"><br><b>17:20-18:50</b> История (309) <img class="emoji" src="/images/together.svg" alt="together"><br>'
];
document.getElementById("text").innerHTML = schedule[day];
const months = [
  "января",
  "февраля",
  "марта",
  "апреля",
  "мая",
  "июля",
  "августа",
  "сентября",
  "октября",
  "ноября",
  "декабря"
];
document.getElementById("month").innerHTML = months[month];
/* Парсер */
